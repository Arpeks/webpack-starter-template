import { ModuleOptions } from 'webpack'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import { BuildOptions } from './types/types'
import ReactRefreshTypeScript from 'react-refresh-typescript'
import { babelBuildLoader } from './babel/babelBuildLoader'

export function buildLoaders(options: BuildOptions): ModuleOptions['rules'] {
	const isDev: boolean = options.mode === 'development'
	const isProd: boolean = options.mode === 'production'

	const assetLoader = {
		test: /\.(png|jpe?g|gif)$/i,
		type: 'asset/resource'
	}

	const svgrLoader = {
		test: /\.svg$/,
		use: [
			{
				loader: '@svgr/webpack',
				options: {
					icon: true,
					svgoConfig: {
						plugins: [
							{
								name: 'convertColors',
								params: {
									currentColor: true
								}
							}
						]
					}
				}
			}
		]
	}

	const scssLoader = {
		test: /\.s[ac]ss$/i,
		use: [
			isDev ? 'style-loader' : MiniCssExtractPlugin.loader,
			{
				loader: 'css-loader',
				options: {
					modules: {
						localIdentName: isDev
							? '[name]_[local]_[hash:base64]'
							: '[hash:base64:8]'
					},
					importLoaders: 1,
					sourceMap: true
				}
			},
			'sass-loader'
		]
	}

	const babelLoader = babelBuildLoader(options)

	const tsLoader = {
		test: /\.tsx?$/,
		use: [
			{
				loader: 'ts-loader',
				options: {
					getCustomTransformers: () => ({
						before: [isDev && ReactRefreshTypeScript()].filter(Boolean)
					}),
					transpileOnly: isDev
				}
			}
		],
		exclude: /node_modules/
	}

	return [
		assetLoader,
		scssLoader,
		// tsLoader,
		babelLoader,
		svgrLoader
	]
}
