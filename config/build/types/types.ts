export type BuildMode = 'production' | 'development'
export type BuildPlatform = 'desktop' | 'mobile'

export interface BuildPaths {
	entry: string
	html: string
	public: string
	output: string
	src: string
}

export interface BuildOptions {
	port: number
	mode: BuildMode
	paths: BuildPaths
	platform?: BuildPlatform
	analyzer?: boolean
}
