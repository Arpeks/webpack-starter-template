import { Configuration } from 'webpack'
import { buildDevServer } from './buildDevServer'
import { buildLoaders } from './buildLoaders'
import { buildPlugins } from './buildPlugins'
import { buildResolvers } from './buildResolvers'
import { BuildOptions } from './types/types'

export function buildWebpack(options: BuildOptions): Configuration {
	const { mode, paths } = options
	const isDev: boolean = mode === 'development'

	return {
		mode: mode ?? 'development',
		entry: paths.entry,
		devtool: isDev ? 'eval-cheap-module-source-map' : 'source-map',
		output: {
			path: paths.output,
			filename: 'bundle/[name].[contenthash:8].bundle.js',
			chunkFilename: 'bundle/chunks/[name].[contenthash:8].chunk.bundle.js',
			clean: true
		},
		plugins: buildPlugins(options),
		module: {
			rules: buildLoaders(options)
		},
		resolve: buildResolvers(options),
		devServer: isDev ? buildDevServer(options) : undefined,
		watchOptions: isDev
			? {
					ignored: /node_modules/
				}
			: undefined
	}
}
