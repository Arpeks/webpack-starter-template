import { buildWebpack } from './config/build/buildWebpack'
import * as path from 'node:path'
import { BuildMode, BuildPlatform } from './config/build/types/types'

interface EnvVariables {
	mode?: BuildMode
	port?: number
	analyzer?: boolean
	platform?: BuildPlatform
}

export default (env: EnvVariables) => {
	return buildWebpack({
		mode: env.mode ?? 'development',
		port: env.port ?? 3000,
		analyzer: env.analyzer ?? false,
		platform: env.platform ?? 'desktop',
		paths: {
			output: path.resolve(__dirname, 'dist'),
			entry: path.resolve(__dirname, 'src', 'index.tsx'),
			html: path.resolve(__dirname, 'public', 'index.html'),
			public: path.resolve(__dirname, 'public'),
			src: path.resolve(__dirname, 'src')
		}
	})
}
