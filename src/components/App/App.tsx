import { useState } from 'react'
import { Link, Outlet } from 'react-router-dom'
import * as styles from './App.module.scss'
import avatarPng from '@/assets/avatar.png'
import avatarJpg from '@/assets/avatar.jpg'
import Calendar from '@/assets/calendar.svg'
import AvatarSvg from '@/assets/app-image.svg'

function TODO() {
	TODO2()
}

function TODO2() {
	throw new Error()
}

const App = () => {
	const [count, setCount] = useState(0)

	const increment = () => {
		setCount(prev => prev + 1)
		TODO()
	}

	// TODO('123')

	return (
		<div data-testid={'App.DataTestId'}>
			<h1 data-testid={'Platform'}>PLATFORM: {__PLATFORM__}</h1>

			<div data-testid={'Images'}>
				<img src={avatarPng} width={100} height={100} alt="avatarPng" />
				<img src={avatarJpg} width={100} height={100} alt="avatarJpg" />
			</div>

			<Calendar width={100} height={100} fill={'red'} />
			<AvatarSvg width={100} height={100} className={styles.icon} />

			<Link to={'/'}>Home</Link>
			<br />
			<Link to={'/about'}>About</Link>
			<br />
			<Link to={'/shop'}>Shop</Link>
			<br />

			<h1 className={styles.value}>{count}</h1>
			<button onClick={increment} className={styles.button}>
				inc
			</button>

			<Outlet />
		</div>
	)
}

export default App
